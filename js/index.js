// Xây dựng chức năng thêm số cho user 
var arrayNum = []  ;
document.getElementById('themso').addEventListener('click' , function(){
    numUserEl = document.querySelector('#txt-number').value * 1 ; 
    arrayNum.push(numUserEl) ; 
    document.querySelector('#inmang').innerHTML = `${arrayNum}` ; 
})

// Xây dựng chức năng tính tổng số dương có trong array 


document.querySelector('#tinhtongsoduong').addEventListener('click' , function(){
    // Cách 1: sử dụng hàm filter để tìm danh sách số dương -> dùng forEach để tính tổng 
    // var sum = 0 ; 
    // arrayNum.filter(number => number > 0).forEach(number => sum += number) ;  
    // document.querySelector('#inketqua1').innerHTML = `Tổng các số dương là : ${sum}` ;
    
    // Cách 2: dùng thủ công để tính tổng số dương 
    var tongCacSoDuong = 0 ; 
    for (var i = 0 ; i < arrayNum.length ;  i++) {
        var eachnumEL = arrayNum[i] ; 
        if (eachnumEL > 0) {
            tongCacSoDuong = tongCacSoDuong + eachnumEL; 
        }
    }
    document.querySelector('#inketqua1').innerHTML = `Tổng các số dương là : ${tongCacSoDuong}` ;

})

// Xây dựng chức năng đếm số dương có trong array 

document.querySelector('#demsoduong').addEventListener('click' , function(){
    // Cách 1: sử dụng hàm map để tìm danh sách số dương -> dùng forEach để lặp 
    // var count = 0 ; 
    // arrayNum.map(number => number > 0 ).forEach(number => count++) ; 
    
    // Cách 2: sử dụng thủ công để đếm số dương 
    var countSoDuong = 0 ; 
    for (var i = 0 ; i < arrayNum.length ;  i++) {
        var eachnumEL = arrayNum[i] ; 
        if (eachnumEL > 0) {
            countSoDuong ++; 
        }
    }
    document.querySelector('#inketqua2').innerHTML = `Tổng các số dương là : ${countSoDuong}` ; 
}) ; 

// Xây dựng chức năng tìm số nhỏ nhất có trong array 

document.querySelector('#timsonhonhat').addEventListener('click' , function(){
    // Cách 1: sử dụng hàm sort để sắp xếp -> tìm ra số lớn nhất hoặc số bé nhất
    // arrayNum.sort((a , b) => b-a) ; 
    // var minValue = arrayNum[arrayNum.length -1] ; 
    // document.querySelector('#inketqua3').innerHTML = `Số nhỏ nhất là: ${minValue}` ; 
    
    // Cách 2: sử dụng forEach để lặp 
    // var minNumber = arrayNum[0] ; 
    // arrayNum.forEach(function(number){
    //     if (number < minNumber) {
    //         minNumber = number ; 
    //     }
    // })
    // document.querySelector('#inketqua3').innerHTML = `Số nhỏ nhất là: ${minNumber}` ; 
    // Cách 3: sử dụng reduce() và toán tử 3 ngôi để tìm số nhỏ nhất 
    // var minValue = arrayNum.reduce(function(current , element) {
    //     return (current < element) ? current : element
    // })
    // console.log(minValue);

    // Cách 5: sử dụng call và apply để tìm số nhỏ nhất 
    // var minValue = Math.min.apply(Math , arrayNum) ; 
    // console.log("🚀 ~ file: index.js:72 ~ document.querySelector ~ minValue", minValue)
    
    // Cách 6: sử dụng vòng lặp for để tìm số lớn nhất hoặc số bé nhất 
    var minValue = arrayNum[0]  ; 
    for (var i = 1 ; i < arrayNum.length ; i++) {
        var current = arrayNum[i] ; 
        if (current < minValue) {
            minValue = current ; 
        }
    }
    document.querySelector("#inketqua3").innerHTML = `Số nhỏ nhất là : ${minValue}` ; 
})


// Xây dựng chức năng tìm số nguyên dương nhỏ nhất có trong array 

document.querySelector("#timsonguyenduongnhonhat").addEventListener('click' , function(){
    var listsonguyenduong = [] ; 
    for(var i = 0 ; i < arrayNum.length ; i++) {
        if (arrayNum[i] > 0) {
            listsonguyenduong.push(arrayNum[i]) ; 
        }
    }
    // Cách 1: Sử dụng hàm sort để sắp xếp -> tìm ra số lớn nhất hoặc số bé nhất 
    // listsonguyenduong.sort((a , b) => a-b) ; 
    // var minValueDuong = listsonguyenduong[0] ; 
    // document.querySelector('#inketqua4').innerHTML = `Số nguyên dương nhỏ nhất là: ${minValueDuong}` ; 
    // Cách 2: sử dụng vòng lặp for để tìm 
    var minValueSoduong = listsonguyenduong[0] ; 
    for (var i =1 ; i < listsonguyenduong.length ; i++) {
        var current = listsonguyenduong[i] ; 
        if (current < minValueSoduong) {
            minValueSoduong = current  ;
        }
    }
    document.querySelector('#inketqua4').innerHTML = `Số nguyên dương nhỏ nhất là: ${minValueSoduong}` ; 


})

// Xây dựng chức năng tìm số chẵn cuối cùng có trong array 

document.querySelector('#timsochancuoicung').onclick = function(){timSoChanCuoiCung()} ; 
function timSoChanCuoiCung() {
    var listsochan = [] ; 
    for(var i = 0 ; i <arrayNum.length ; i++) {
        if(arrayNum[i] % 2 == 0) {
            listsochan.push(arrayNum[i]) ; 
        }
    }
    if (listsochan.length == 0) {
        document.querySelector("#inketqua5").innerHTML = `-1. Bạn không nhập số chẵn nào cả` ; 
    }else {
        var sochancuoicung = listsochan[listsochan.length - 1] ; 
        document.querySelector("#inketqua5").innerHTML = `Số chẵn cuối cùng là: ${sochancuoicung}` ; 

    }

}

// Xây dựng chức năng sắp xếp mảng theo thứ tự tăng dần 

document.querySelector('#sapxeptangdan').onclick = function(){sapXepTangDan()} ; 
function sapXepTangDan() {
    // Cách 1: sử dụng hàm sort để sắp xếp 
    // var arrayNumAfter = arrayNum.sort((a , b) => a-b) ; 
    // document.querySelector('#inketqua6').innerHTML = `Mảng sau khi sắp: ${arrayNumAfter}` ; 
    // Cách 2: sử dụng vòng lặp for lồng để sắp xếp
    for (var i = 0 ; i < arrayNum.length ; i++) {
        for (var j = i +1 ; j < arrayNum.length ; j++) {
            if (arrayNum[i] > arrayNum[j]) {
                var temp = arrayNum[i] ; 
                arrayNum[i] = arrayNum[j] ; 
                arrayNum[j] = temp  ;
            }
        }
    }
    document.querySelector('#inketqua6').innerHTML = `Mảng sau khi sắp : ${arrayNum}` ; 

}

// Xây dựng chức năng tìm số nguyên tố đầu tiên có trong array 

// Hàm kiểm tra xem số đó có phải số nguyên tố hay không 

function KiemTraSoNguyenTo(n) {
    // Cờ hiệu để kiểm tra 
    var flag = true ; 
    if (n < 2) {
        flag = false ; 
    }
    else if (n == 2) {
        flag = true ; 
    }
    else if (n %2 == 0) {
        flag = false ; 
    }
    else {
        for( var i = 3 ; i <= Math.sqrt(n) ; i += 2) {
            if (n % i == 0) {
                flag = false ; 
                break ; 
            }
        }
    }
    return flag ; 
}

document.querySelector('#timsonguyentodautien').onclick = function(){timSoNguyenToDauTien()} ; 
function timSoNguyenToDauTien() {
    var listsonguyento = [] ; 
    for(var i = 0 ; i < arrayNum.length ; i++) {
        var eachIndex = arrayNum[i] ; 
        if (KiemTraSoNguyenTo(eachIndex)) {
            listsonguyento.push(eachIndex) ; 
        }
    }
    if (listsonguyento.length == 0) {
        document.querySelector('#inketqua7').innerHTML = `-1.Array của bạn không có số nguyên tố` ; 
    }else {
        var songuyentodautien = listsonguyento[0] ; 
        document.querySelector('#inketqua7').innerHTML = `Số nguyên tố đầu tiên là : ${songuyentodautien}` ; 
    }
}

// Xây dựng chức năng tìm số nguyên có trong array thứ 2 

// Xác định array thứ 2 và in ra ngoài 

var arrayNum2 = []  ;
document.getElementById('themso2').addEventListener('click' , function(){
    numUserEl2 = document.querySelector('#txt-number2').value * 1 ; 
    arrayNum2.push(numUserEl2) ; 
    document.querySelector('#inmang2').innerHTML = `${arrayNum2}` ; 
})

// Xác định số nguyên có trong array thứ 2 

document.querySelector('#demsonguyen').addEventListener('click' , function(){
    var count2 = 0 ; 
    for (var i = 0 ; i < arrayNum2.length ; i++) {
        eachIndex2 = arrayNum2[i] ; 
        if (Number.isInteger(eachIndex2)) {
            count2++ ;
        }
    }
    document.querySelector('#inketqua8').innerHTML = `Có ${count2} số nguyên` ; 
})

// Xây dựng chức năng so sánh số dương và số âm trong array thứ 1 

document.querySelector('#sosanh').addEventListener('click' , function(){
    var listsoduong = [] ; 
    var listsoam = []  ;
    for (var i = 0 ; i <arrayNum.length ; i++) {
        var eachIndex3 = arrayNum[i] ; 
        if (eachIndex3 >= 0 ) {
            listsoduong.push(eachIndex3) ; 
        }else {
            listsoam.push(eachIndex3) ; 
        }
    }
    if (listsoduong.length > listsoam.length) {
        document.querySelector('#inketqua9').innerHTML = `số dương lớn hơn số âm` ; 
    }else if (listsoduong.length < listsoam.length) {
        document.querySelector('#inketqua9').innerHTML = `số âm lớn hơn số dương` ; 
    }else {
        document.querySelector('#inketqua9').innerHTML = `số âm bằng số dương` ; 
    }
})

// Xây dựng chức năng đổi chỗ trong array 

document.querySelector('#doicho').addEventListener('click' ,function(){
    var viTri1 = document.querySelector('#vitri1').value * 1 ; 
    var viTri2 = document.querySelector('#vitri2').value * 1 ; 
    // Cách 1: sử dụng 2 biến để hoán đổi vị trí cho nhau 
    // var Giatri1 = arrayNum[viTri1] ; 
    // var Giatri2 = arrayNum[viTri2] ; 
    // arrayNum[viTri2] = Giatri1 ; 
    // arrayNum[viTri1] = Giatri2 ; 
    // Cách 2 : sử dụng 1 biến để hoán đổi vị trí cho nhau 
    var trungGian = arrayNum[viTri1] ; 
    arrayNum[viTri1] = arrayNum[viTri2] ;
    arrayNum[viTri2] = trungGian ; 
    document.querySelector('#inketqua10').innerHTML = `Mảng sau khi đổi chỗ là : ${arrayNum}` ; 
})